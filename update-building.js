/* eslint-disable no-unused-vars */
const mongoose = require('mongoose')
mongoose.connect('mongodb://locolhost:27017/example')
const Room = require('./models/Romm')
const Building = require('./models/Building')

async function main () {
  // const room = await Room.findById('621611555816c4f9cec8033')
  // room.capacity = 20
  // room.save()
  // console.log(room)
  // const room = await Room.findOne({ capacity: { $gte: 100 } })
  // console.log(room)
  // console.log('---------------')
  // const rooms = await Room.find({ capacity: { $gte: 100 } })
  // console.log(rooms)
  const newInformaticsBuilding = await Building.findById('621611555816c4f9cec8033')
  // eslint-disable-next-line no-use-before-define
  const room = await Room.findById('621611555816c4f9cec8033')
  const informaticsBuilding = await Building.findById(room.building)
  console.log(newInformaticsBuilding)
  console.log(room)
  console.log(informaticsBuilding)
  room.building = newInformaticsBuilding
  newInformaticsBuilding.rooms.push(room)
  newInformaticsBuilding.rooms.pull(room)
  room.save()
  newInformaticsBuilding.save()
  informaticsBuilding.save()
}

main().then(() => {
  console.log('Finish')
})
